-- Copyright © 2019 by Pierre-Antoine Rouby <contact@parouby.fr>
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>

function love.mousereleased (x, y, button)
   -- Whene mouse is released
   if button == 1 then
      -- Add mouse pos (with some random) on a1 table to display
      table.insert(a1, {x = (x + math.random(50) - 25) - 32,
                        y = y})
      -- More cookies!
      nb_cookies = nb_cookies + 1
   end
end

function love.load()
   love.window.setTitle("Give me cookies - LÖVE")

   local width, height, flags = love.window.getMode()
   love.window.setMode(720, 720, flags)

   -- Font
   font = love.graphics.newFont("GrapeSoda.ttf", 64)
   love.graphics.setFont (font)

   -- Assets
   cookies_sx = (720 / 2560)
   cookies_sy = (720 / 2560)
   cookies = love.graphics.newImage("cookies.png")

   -- Global variables
   a1 = {}
   nb_cookies = 0
end

function love.draw()
   love.graphics.setBackgroundColor (128/255, 200/255, 255/255, 1)
   love.graphics.draw(cookies, 0, 0, 0, cookies_sx, cookies_sy)

   love.graphics.print("Score: " .. nb_cookies, 280, 0)

   -- Display +1
   for k, v in pairs(a1) do
      love.graphics.print("+1", v["x"], v["y"])
      if v["y"] < -64 then
         table.remove(a1, k)
      else
         v["y"] =  v["y"] - 4
      end
   end
end
