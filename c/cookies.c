/* Copyright © 2019 by Pierre-Antoine Rouby <contact@parouby.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define WIDTH  720
#define HEIGHT 720
#define TITLE  "Give me cookies - SDL2"
#define TXT    "+1"

#define SDL_ASSERT(exp)                                         \
  if (!(exp))                                                   \
    {                                                           \
      fprintf(stderr, "[error][SDL] %s", SDL_GetError());       \
      abort();                                                  \
    }

/*********/
/* TYPES */
/*********/

struct listed_txt_s {
  int x;
  int y;
  struct listed_txt_s * next;
};
typedef struct listed_txt_s * list_txt;

/**********/
/* GLOBAL */
/**********/

SDL_Window *window;
SDL_Renderer *renderer;
TTF_Font * font;
SDL_Texture * cookie_tex;
SDL_Rect cookie_rect;
SDL_Texture * a1_tex;
SDL_Rect a1_rect;

unsigned cookies = 0;
list_txt a1 = NULL;

/********/
/* LIST */
/********/

list_txt
add_lt (list_txt lst, int x, int y)
{
  list_txt new = malloc (sizeof(struct listed_txt_s));
  new->x = x;
  new->y = y;
  new->next = lst;
  return new;
}

list_txt
map_lt (list_txt lst)
{
  if (lst == NULL)
    return NULL;

  /* If lst need to be delete, return magic number */
  if (lst->y < -64)
    return (list_txt)0x0001;

  list_txt ret;

  lst->y -= 4;

  a1_rect.x = lst->x;
  a1_rect.y = lst->y;

  SDL_RenderCopy (renderer, a1_tex,
                  NULL, &a1_rect);

  ret = map_lt (lst->next);
  if (ret == (list_txt)0x0001)
    {
      /* Remove next element and retry */
      list_txt old = lst->next;
      lst->next = old->next;
      free (old);
      ret = map_lt (lst->next);
    }
  return ret;
}

/*********/
/* TOOLS */
/*********/

void
draw_score ()
{
  char text [255];

  sprintf (text, "Score: %d", cookies);

  SDL_Color color = { 255, 255, 255, 255 };
  SDL_Surface * surf = TTF_RenderText_Blended(font, text, color);
  SDL_Texture * tex = SDL_CreateTextureFromSurface(renderer, surf);
  SDL_Rect rect;

  SDL_QueryTexture(tex, NULL, NULL, &rect.w, &rect.h);

  rect.x = 280;
  rect.y = 0;

  SDL_RenderCopyEx(renderer, tex, NULL, &rect, 0, NULL, SDL_FLIP_NONE);
  SDL_FreeSurface(surf);
}

/********/
/* INIT */
/********/

void
init ()
{
  srand (time(NULL));

  cookie_tex = IMG_LoadTexture(renderer, "cookies.png");
  SDL_ASSERT(cookie_tex != NULL);

  cookie_rect.x = 0;
  cookie_rect.y = 0;
  cookie_rect.w = WIDTH;
  cookie_rect.h = HEIGHT;

  font = TTF_OpenFont("GrapeSoda.ttf", 64);
  SDL_ASSERT(font != NULL);
  SDL_Surface * surf = TTF_RenderText_Blended(font, TXT,
                                              (SDL_Color) {255, 255, 255, 255});
  a1_tex = SDL_CreateTextureFromSurface(renderer, surf);
  SDL_FreeSurface (surf);

  SDL_QueryTexture(a1_tex, NULL, NULL, &a1_rect.w, &a1_rect.h);
}

/********/
/* DRAW */
/********/

void
draw ()
{
  SDL_SetRenderDrawColor (renderer, 128, 200, 255, 255);
  SDL_RenderClear (renderer);

  int x, y;
  SDL_Event event;

  while (SDL_PollEvent (&event))
    {
      switch (event.type)
        {
        case SDL_QUIT:
          return;
        case SDL_MOUSEBUTTONUP:
          cookies += 1;
          SDL_GetMouseState (&x, &y);
          a1 = add_lt(a1, x + ((rand() % 50) - 25), y);
          break;
        }
    }

  SDL_RenderCopy (renderer, cookie_tex,
                  NULL, &cookie_rect);

  draw_score();
  map_lt(a1);

  SDL_RenderPresent (renderer);
  draw ();                      /* Recursion */
}

int
main (/* int argc, char ** argv */)
{
  int ret = 0;

  ret = SDL_Init (SDL_INIT_VIDEO);
  SDL_ASSERT(ret == 0);

  ret = IMG_Init(IMG_INIT_PNG);
  SDL_ASSERT((ret & IMG_INIT_PNG) == IMG_INIT_PNG);

  ret = TTF_Init();
  SDL_ASSERT(ret == 0);

  /* Window */
  window = SDL_CreateWindow(TITLE,
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            WIDTH, HEIGHT,
                            SDL_WINDOW_SHOWN);
  SDL_ASSERT(window != NULL);

  /* Renderer */
  renderer = SDL_CreateRenderer(window, -1,
                                SDL_RENDERER_ACCELERATED
                                | SDL_RENDERER_PRESENTVSYNC);
  SDL_ASSERT(renderer != NULL);

  init();
  draw();

  SDL_Quit();
  IMG_Quit();
  TTF_Quit();

  return EXIT_SUCCESS;
}
