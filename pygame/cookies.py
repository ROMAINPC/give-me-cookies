## Copyright © 2019 by Pierre-Antoine Rouby <contact@parouby.fr>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>

import pygame

successes, failures = pygame.init()
if failures != 0:
    print ("Failed to init pygame")
    quit()

pygame.display.set_caption("Give me cookies - Pygame")
screen = pygame.display.set_mode((720, 720))
clock = pygame.time.Clock()
FPS = 60
background = pygame.surface.Surface((720, 720))
background_color = (128, 200, 255)
background.fill (background_color)

sprite = pygame.transform.scale(pygame.image.load ("cookies.png"),
                                (720, 720))
sprite_rect = sprite.get_rect()

font = pygame.font.Font("GrapeSoda.ttf", 64)

cookies = 0

text_add_one = "+1"
surface_add_one = font.render(text_add_one, True, (255, 255, 255))
list_add_one = []

while True:
    # Get event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit()
        if event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                list_add_one.append((event.pos[0], event.pos[1]))
                cookies += 1

    # Display
    screen.blit(background, sprite_rect)
    screen.blit(sprite, sprite_rect)

    newlst = []
    for p in list_add_one:
        x = p[0]
        y = p[1] - 4
        screen.blit (surface_add_one, (x, y))
        if y > -64:
            newlst.append((x, y))
    list_add_one = newlst

    score = font.render ("Score: {0}".format (cookies), True, (255, 255, 255))
    screen.blit (score, (200, 0))

    # Update display and wait
    pygame.display.update()
    clock.tick(FPS)
