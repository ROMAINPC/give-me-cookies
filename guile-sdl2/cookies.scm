#!@GUILE@ --no-auto-compile
-*- scheme -*-
!#
;; Copyright © 2019 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(use-modules (srfi srfi-1)
             (srfi srfi-9)
             (srfi srfi-28)
             (ice-9 match)
             (ice-9 rdelim)
             (sdl2)
             (sdl2 ttf)
             (sdl2 render)
             (sdl2 surface)
             (sdl2 events)
             (sdl2 image)
             (sdl2 video)
             (sdl2 input mouse))

(define game-width 720)
(define game-height 720)

(define cookies-img #f)
(define font #f)
(define white  '(230 230 230 255))

(define (write-score score)
  (let ((port (open-file (string-append (getenv "HOME")
                                        "/.cookies.score")
                         "w")))
    (display (format "~a\n"
                     score)
             port)
    (close-port port)))

(define (read-score)
  (let ((port (open-file (string-append (getenv "HOME")
                                        "/.cookies.score")
                         "r")))
    (let ((score (string->number (car (%read-line port)))))
      (close-port port)
      score)))

(define (mouse-down-pressed? e)
  (and (mouse-button-down-event? e)
       (eq? (mouse-button-event-button e) 'left)))

(define (get-events)
  (let ((event (poll-event)))
    (cond
     ((quit-event? event)         'quit)
     ((mouse-down-pressed? event) 'click)
     ((not event)                 'continue)
     (else (get-events)))))


(define* (display-text render text color
                       #:key
                       (display-font font)
                       (centred-x? #f)
                       (centred-y? #f)
                       (margin-w 5)
                       (margin-h 5)
                       (pos-x 0)
                       (pos-y 0))
  (let* ((surface (render-font-solid display-font text color))
         (texture (surface->texture render surface))
         (sw (surface-width  surface))
         (sh (surface-height surface))
         (pos-x (if centred-x?
                    (round (- (/ game-width 2)
                              (/ sw 2)))
                    (+ pos-x margin-w)))
         (pos-y (if centred-y?
                    (round (- (/ game-height 2)
                              (/ sh 2)))
                    (- pos-y margin-h sh))))
    (render-copy render texture
                 #:dstrect (list pos-x
                                 pos-y
                                 sw sh))
    (delete-texture! texture)
    (delete-surface! surface)))

(define (background ren)
  (set-render-draw-color ren 128 200 255 255)
  (clear-renderer ren))

(define (display-add ren add)
  (match add
    ((click x y)
     (if (< y 0)
         #f
         (begin
           (display-text ren "+1" (apply make-color white)
                         #:pos-x x
                         #:pos-y y)
           (list 'click x (- y 4)))))
    (_ #f)))

(define (draw ren score adds)
  (background ren)
  (render-copy ren cookies-img #:dstrect `(0 0 ,game-width ,game-height))
  (display-text ren (format "Score: ~a" score)
                (apply make-color white)
                #:centred-x? #t
                #:pos-y 60)
  (let ((adds (remove not
                      (map (λ (add)
                             (display-add ren add))
                           adds))))
    (present-renderer ren)
    (let ((event (get-events)))
      (cond
       ((equal? event 'quit)  (write-score score) #f)
       ((equal? event 'click) (draw ren (1+ score)
                                    (cons `(click
                                            ,(- (+ (mouse-x) (random 30)) 15)
                                            ,(mouse-y)) adds)))
       (else                  (draw ren score adds))))))

(define (init ren)
  (set! cookies-img (surface->texture ren (load-image "cookies.png")))
  (set! font (load-font "GrapeSoda.ttf" 64))
  (if (file-exists? (string-append (getenv "HOME")
                                   "/.cookies.score"))
      (draw ren (read-score) '())
      (draw ren 0 '())))

(sdl-init)
(ttf-init)

(call-with-window
 (make-window #:title "Give me cookies - Guile-SDL2"
              #:size (list game-width game-height))
 (lambda (window)
   (call-with-renderer (make-renderer window) init)))

(ttf-quit)
(sdl-quit)
